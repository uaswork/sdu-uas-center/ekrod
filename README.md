# EKROD

Definition of the restricted airspace EK R OD 1-3 as specified in AIP SUP 10/17 25 MAY 2017.
For any questions or comments please contact:

Kjeld Jensen
SDU UAS Center
University of Southern Denmmark
http://sdu.dk/uas
kjen@mmmi.sdu.dk

