#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#/****************************************************************************
# Restricted airspace EK R OD 1-5 coordinates and defines.
# Copyright (c) 2019-2022, Kjeld Jensen <kjen@mmmi.sdu.dk>
# SDU UAS Center http://sdu.dk/uas
# University of Southern Denmark
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************/
'''
This file contains coordinates and defines for the restricted airspace
EK R OD 1-5 specified in the AIP SUP 02/22 25 JAN 2022.

2019-04-30 Kjeld First version.
2020-12-22 Kjeld Fixed decimal seconds bug. Added print function.
                 Added ekrod1_2, ekrod1_3 and ekrod2_3
2022-01-24 Kjeld Added EK R OD4 and EK R OD5
'''

# defines
feet2meter = 0.3048

# EK R OD1, OD2, OD3
ekrod_alt_3500 = 3500 * feet2meter

# EK R OD4, OD5
ekrod_alt_1500 = 1500 * feet2meter

# EK R OD1
# 55 23 30N 010 15 00E -
# 55 29 30N 010 08 45E -
# 55 34 30N 010 24 00E -
# 55 28 30N 010 30 15E

ekrod1 = [[55 + 23/60. + 30/3600., 10 + 15/60. +  0/3600.],
          [55 + 29/60. + 30/3600., 10 +  8/60. + 45/3600.], 
          [55 + 34/60. + 30/3600., 10 + 24/60. +  0/3600.],
          [55 + 28/60. + 30/3600., 10 + 30/60. + 15/3600.]]

# EK R OD2
# 55 29 30N 010 08 45E -
# 55 36 00N 010 02 15E -
# 55 36 00N 010 12 30E -
# 55 37 00N 010 17 00E -
# 55 37 00N 010 21 30E -
# 55 34 30N 010 24 00E

ekrod2 = [[55 + 29/60. + 30/3600., 10 +  8/60. + 45/3600.],
          [55 + 36/60. +  0/3600., 10 +  2/60. + 15/3600.], 
          [55 + 36/60. +  0/3600., 10 + 12/60. + 30/3600.],
          [55 + 37/60. +  0/3600., 10 + 17/60. +  0/3600.],
          [55 + 37/60. +  0/3600., 10 + 21/60. + 30/3600.],
          [55 + 34/60. + 30/3600., 10 + 24/60. +  0/3600.]]

# EK R OD3
# 55 36 00N 009 56 00E -
# 55 44 00N 009 56 00E -
# 55 44 00N 010 24 30E -
# 55 39 00N 010 27 30E -
# 55 37 00N 010 25 00E -
# 55 37 00N 010 17 00E -
# 55 36 00N 010 12 30E

ekrod3 = [[55 + 36/60. +  0/3600.,  9 + 56/60. +  0/3600.],
          [55 + 44/60. +  0/3600.,  9 + 56/60. +  0/3600.], 
          [55 + 44/60. +  0/3600., 10 + 24/60. + 30/3600.],
          [55 + 39/60. +  0/3600., 10 + 27/60. + 30/3600.],
          [55 + 37/60. +  0/3600., 10 + 25/60. +  0/3600.],
          [55 + 37/60. +  0/3600., 10 + 17/60. +  0/3600.],
          [55 + 36/60. +  0/3600., 10 + 12/60. + 30/3600.]]

# EK R OD4
# 55 37 00N 010 25 00E -
# 55 39 00N 010 27 30E -
# 55 44 00N 010 24 30E -
# 55 44 00N 010 34 00E -
# 55 53 20N 010 45 15E -
# 55 48 12N 011 00 36E -
# 55 37 00N 010 49 00E -
# 55 37 00N 010 25 00E.

ekrod4 = [[55 + 37/60. +  0/3600., 10 + 25/60. +  0/3600.],
          [55 + 39/60. +  0/3600., 10 + 27/60. + 30/3600.], 
          [55 + 44/60. +  0/3600., 10 + 24/60. + 30/3600.],
          [55 + 44/60. +  0/3600., 10 + 34/60. +  0/3600.],
          [55 + 53/60. + 20/3600., 10 + 45/60. + 15/3600.],
          [55 + 48/60. + 12/3600., 11 +  0/60. + 36/3600.],
          [55 + 37/60. +  0/3600., 10 + 49/60. +  0/3600.],
          [55 + 37/60. +  0/3600., 10 + 25/60. +  0/3600.]]

# EK R OD5
# 55 53 20N 010 45 15E -
# 56 06 58N 011 01 41E -
# 55 55 08N 011 07 41E -
# 55 48 12N 011 00 36E -
# 55 53 20N 010 45 15E.

ekrod5 = [[55 + 53/60. + 20/3600., 10 + 45/60. + 15/3600.],
          [56 +  6/60. + 58/3600., 11 +  1/60. + 41/3600.], 
          [55 + 55/60. +  8/3600., 11 +  7/60. + 41/3600.],
          [55 + 48/60. + 12/3600., 11 +  0/60. + 36/3600.],
          [55 + 53/60. + 20/3600., 10 + 45/60. + 15/3600.]]

# EK R OD1 and OD2
ekrod1_2 = [ekrod1[0], ekrod2[0], ekrod2[1], ekrod2[2], ekrod2[3], ekrod2[4], ekrod2[5], ekrod1[3]]

# EK R OD1 and OD2 and OD3
ekrod1_3 = [ekrod1[0], ekrod2[0], ekrod2[1], ekrod3[0], ekrod3[1], ekrod3[2], ekrod3[3], ekrod3[4], ekrod2[4], ekrod2[5], ekrod1[3]]

# EK R OD2 and OD3
ekrod2_3 = [ekrod2[0], ekrod2[1], ekrod3[0], ekrod3[1], ekrod3[2], ekrod3[3], ekrod3[4], ekrod2[4], ekrod2[5]]


def print_coords (name, coords, alt):
	print (name)
	print ('[', end='')
	for i in range(len(coords)):
		print ('[%.7f,%.7f,%.1f]' % (coords[i][1],coords[i][0],alt), end='')
		if i < len(coords) - 1:
			print (',', end='')
	print (']\n')

if __name__ == '__main__':
	print_coords ('EK R OD1', ekrod1, ekrod_alt_3500)
	print_coords ('EK R OD2', ekrod2, ekrod_alt_3500)
	print_coords ('EK R OD3', ekrod3, ekrod_alt_3500)
	print_coords ('EK R OD4', ekrod4, ekrod_alt_1500)
	print_coords ('EK R OD5', ekrod5, ekrod_alt_1500)
	print_coords ('EK R OD1-2', ekrod1_2, ekrod_alt_3500)
	print_coords ('EK R OD1-3', ekrod1_3, ekrod_alt_3500)
	print_coords ('EK R OD2-3', ekrod2_3, ekrod_alt_3500)

