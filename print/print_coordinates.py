#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#/****************************************************************************
# Print EK R OD 1-5 coordinates
# Copyright (c) 2019-2022, Kjeld Jensen <kjen@mmmi.sdu.dk>
# SDU UAS Center http://sdu.dk/uas
# University of Southern Denmark
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************/
'''
This file contains a simple script to print the coordinates of the
restricted airspace EK R OD 1-5.


2019-04-30 Kjeld First version
2020-12-22 Kjeld Added 1_2, 1_3 and 2_3 airspaces.
2022-01-24 Kjeld Added support for EK R OD4 and EK R OD5. Removed two decimals.
'''

import sys
sys.path.append('../')

from ekrod import *

def print_coords (name, coords, alt):
	print (name)
	print ('Latitude   Longitude   Altitude')
	for i in range(len(coords)):
		print ('%08.5f %09.5f %.0f' % (coords[i][0], coords[i][1], alt))
	print ('')

if __name__ == '__main__':
	print_coords ('EK R OD1', ekrod1, ekrod_alt_3500)
	print_coords ('EK R OD2', ekrod2, ekrod_alt_3500)
	print_coords ('EK R OD3', ekrod3, ekrod_alt_3500)
	print_coords ('EK R OD4', ekrod4, ekrod_alt_1500)
	print_coords ('EK R OD5', ekrod5, ekrod_alt_1500)
	print_coords ('EK R OD1-2', ekrod1_2, ekrod_alt_3500)
	print_coords ('EK R OD1-3', ekrod1_3, ekrod_alt_3500)
	print_coords ('EK R OD2-3', ekrod2_3, ekrod_alt_3500)

