#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#/****************************************************************************
# Save EK R OD 1-3 coordinates as KML files
# Copyright (c) 2019-2022, Kjeld Jensen <kjen@mmmi.sdu.dk>
# SDU UAS Center http://sdu.dk/uas
# University of Southern Denmark
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************/
'''
This file contains a script to save the coordinates of the restricted
airspace EK R OD 1-5 as KML files

2019-04-30 Kjeld First version
2020-12-22 Kjeld Added function to save kml. 1_2, 1_3 and 2_3 airspaces.
2022-01-24 Kjeld Added support for EK R OD4 and EK R OD5
'''

import sys
sys.path.append('../')

from ekrod import *
from exportkml import kmlclass


kml = kmlclass()

def save_kml (file_name, name, desc, coords, alt):
	kml.begin(file_name, name, desc, 1.0)
	kml.polybegin(name, '','red_poly', 'absolute')
	for i in range(len(coords)):
		lat = coords[i][0]
		lon = coords[i][1]
		kml.pt(lat, lon, alt)
	kml.polyend()
	kml.end()

save_kml ('ekrod1.kml', 'EK R OD 1', 'Restricted airspace EK R OD 1', ekrod1, ekrod_alt_3500)
save_kml ('ekrod2.kml', 'EK R OD 2', 'Restricted airspace EK R OD 2', ekrod2, ekrod_alt_3500)
save_kml ('ekrod3.kml', 'EK R OD 3', 'Restricted airspace EK R OD 3', ekrod3, ekrod_alt_3500)
save_kml ('ekrod4.kml', 'EK R OD 4', 'Restricted airspace EK R OD 4', ekrod4, ekrod_alt_1500)
save_kml ('ekrod5.kml', 'EK R OD 5', 'Restricted airspace EK R OD 5', ekrod5, ekrod_alt_1500)
save_kml ('ekrod1_2.kml', 'EK R OD 1-2', 'Restricted airspace EK R OD 1 and 2', ekrod1_2, ekrod_alt_3500)
save_kml ('ekrod1_3.kml', 'EK R OD 1-3', 'Restricted airspace EK R OD 1 and 2 and 3', ekrod1_3, ekrod_alt_3500)
save_kml ('ekrod2_3.kml', 'EK R OD 2-3', 'Restricted airspace EK R OD 2 and 3', ekrod2_3, ekrod_alt_3500)

